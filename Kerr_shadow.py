import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker, cm
from scipy.optimize import fsolve
import math


epsilon = 1e-9



# metric
def Delta(M, a, r):
    return r**2 + a**2 - 2*M*r

# photon orbits
def r_ph_1(M, a):
    return 2*M * (1 + np.cos(2/3 * np.arccos(-a/M)))

def r_ph_2(M, a):
    return 2*M * (1 + np.cos(2/3 * np.arccos(a/M)))

# motion constants
def xi(M, a, r):
    return (M*(r**2 - a**2) - r*Delta(M, a, r)) / (a*(r - M))

def eta(M, a, r):
    return r**3 / (a**2 * (r-M)**2) * (4 * a**2 * M - r * (r - 3*M)**2)

# sky positions
def alpha(theta_O, xi):
    return -xi / np.sin(theta_O)

def beta(a, theta_O, xi, eta):
    return np.sqrt(eta + a**2 * np.cos(theta_O)**2 - xi**2 / (np.tan(theta_O)**2))

# Schwarzschild shadow radius
def Schwarzschild_rho(M):
    return np.sqrt(27) * M

# polar photon orbit
def polar_radius(M, a):
    if a == 0:
        return Schwarzschild_rho(M)
    arg = 1/3 * np.arccos((3 * np.sqrt(3) * (M**2 - a**2)) / (3*M**2 - a**2)**(3/2))
    r_xi0 = M + 2 * np.sqrt(M**2 - a**2 / 3) * np.cos(arg)
    return np.sqrt(eta(M, a, r_xi0) + a**2)

# numerical solution for r range
def r_range(M, a, theta_O):
    f = lambda x : eta(M, a, x) + a**2 * np.cos(theta_O)**2 - xi(M, a, x)**2 / np.tan(theta_O)**2
    r_ph_1_ = r_ph_1(M, a)
    r_ph_2_ = r_ph_2(M, a)
    r_1 = fsolve(f, r_ph_1_, xtol=1e-10)
    r_2 = fsolve(f, r_ph_2_, xtol=1e-10)

    return np.array([
        r_1[0], r_2[0]
    ])

# displacment
def D(M, a, theta_O):
    r_range_ = r_range(M, a, theta_O)
    alpha_1 = alpha(theta_O, xi(M, a, r_range_[0] + epsilon))
    alpha_2 = alpha(theta_O, xi(M, a, r_range_[1] - epsilon))

    return 0.5 * np.abs(alpha_1 + alpha_2)

# horizontal diameter
def delta_alpha(M, a, theta_O):
    if a == 0:
        return 2*Schwarzschild_rho(M)
    if theta_O == 0:
        return 2*polar_radius(M, a)
    
    r_range_ = r_range(M, a, theta_O)
    alpha_1 = alpha(theta_O, xi(M, a, r_range_[0] + epsilon))
    alpha_2 = alpha(theta_O, xi(M, a, r_range_[1] - epsilon))

    return np.abs(alpha_1 - alpha_2)

# r value for max beta
def r_max_beta(M, a, theta_O):
    if a == 0:
        return None
    if theta_O == 0:
        return None

    return M + 2 * np.sqrt(M**2 - a**2 * np.cos(theta_O)**2 / 3) * np.cos(
        np.arccos((np.sqrt(27) * (M**2 - a**2 * np.cos(theta_O)**2)) / (3*M**2 - a**2 * np.cos(theta_O)**2)**(3/2)) / 3
    )
    
# vertical diameter
def delta_beta(M, a, theta_O):
    if a == 0:
        return 2*Schwarzschild_rho(M)
    if theta_O == 0:
        return 2*polar_radius(M, a)

    r = r_max_beta(M, a, theta_O)

    return 2*beta(a, theta_O, xi(M, a, r), eta(M, a, r))

# chi
def chi(M, a, theta_O):
    r_range_ = r_range(M, a, theta_O)
    alpha_1 = alpha(theta_O, xi(M, a, r_range_[0] + epsilon))
    alpha_2 = alpha(theta_O, xi(M, a, r_range_[1] - epsilon))

    return 2 * np.sqrt(27) * M / np.abs(alpha_1 - alpha_2)

# curvature radius
def R_C(M, a, theta_O, r):
    if a == 0:
        return Schwarzschild_rho(M)
    if theta_O == 0:
        return polar_radius(M, a)

    res = 64 * np.sqrt(M) * (r**3 - a**2 * r * np.cos(theta_O)**2)**(3/2)
    res *= (r * (r**2 - 3*M*r + 3*M**2) - a**2 * M**2)
    res /= (r - M)**3
    res /= (3 * (8 * r**4 - a**4 - 8*a**2*r**2) - 4*a**2 * (6*r**2 + a**2) * np.cos(2*theta_O) - a**4 * np.cos(4*theta_O))
    return res

def R_C_LRT(M, a, theta_O):
    r_range_ = r_range(M, a, theta_O)
    R_CL = R_C(M, a, theta_O, r_range_[0] + epsilon)
    R_CR = R_C(M, a, theta_O, r_range_[1] - epsilon)

    r_max_beta_ = r_max_beta(M, a, theta_O)
    R_CT = R_C(M, a, theta_O, r_max_beta_)

    return np.array([
        R_CL,
        R_CR,
        R_CT
    ])

# fit circle to three points
def circle_fit(p1, p2, p3):
    # find perpendicular between p1, p2
    a1 = [
        -(p2[1] - p1[1]),
        p2[0] - p1[0]
    ]
    b1 = [
        (p1[0] + p2[0]) / 2,
        (p1[1] + p2[1]) / 2
    ]
    
    # find perpendicular between p2, p3
    a2 = [
        -(p3[1] - p2[1]),
        p3[0] - p2[0]
    ]
    b2 = [
        (p3[0] + p2[0]) / 2,
        (p3[1] + p2[1]) / 2
    ]

    # solve
    # a1[0] t - a2[0] s = b2[0] - b1[0]
    # a1[1] t - a2[1] s = b2[1] - b1[1]
    det = (a1[0] * -a2[1]) - (-a2[0] * a1[1])
    a = -a2[1] / det
    b = a2[0] / det
    c = -a1[1] / det
    d = a1[0] / det

    t = a * (b2[0] - b1[0]) + b * (b2[1] - b1[1])
    s = c * (b2[1] - b1[0]) + d * (b2[1] - b1[1])

    center = [
        a1[0] * t + b1[0],
        a1[1] * t + b1[1]
    ]
    radius = np.sqrt((center[0] - p1[0])**2 + (center[1] - p1[1])**2)

    return center, radius

# fitted circle quantities
def R_S_delta_S(M, a, theta_O):
    if a == 0:
        return Schwarzschild_rho(M), 0
    if theta_O == 0:
        return polar_radius(M, a), 0
    
    r_range_ = r_range(M, a, theta_O)
    r = r_max_beta(M, a, theta_O)

    alpha_R = alpha(theta_O, xi(M, a, r_range_[1] - epsilon))
    beta_T = beta(a, theta_O, xi(M, a, r), eta(M, a, r))
    alpha_T = alpha(theta_O, xi(M, a, r))
    alpha_L = alpha(theta_O, xi(M, a, r_range_[0] + epsilon))

    center, radius = circle_fit(
        [alpha_R, 0],
        [alpha_T, beta_T],
        [alpha_T, -beta_T]
    )
    delta = (alpha_L - (center[0] - radius)) / radius

    return radius, delta





# computation of shadows
def Shadows():

    theta_O = np.pi / 6.0

    metrics = [
        [ 1.0, 0.1 ],
        [ 1.0, 0.2 ],
        [ 1.0, 0.3 ],
        [ 1.0, 0.4 ],
        [ 1.0, 0.5 ],
        [ 1.0, 0.6 ],
        [ 1.0, 0.7 ],
        [ 1.0, 0.8 ],
        [ 1.0, 0.9 ],
        [ 1.0, 1.0 ]
    ]

    plt.figure()
    for j in range(0, len(metrics)):
        M = metrics[j][0]
        a = metrics[j][1]
        
        r1 = r_ph_1(M, a)
        r2 = r_ph_2(M, a)

        r_range_ = r_range(M, a, theta_O)

        print(r1, r2)
        print(r_range_[0], r_range_[1])

        r_ = np.linspace(r_range_[0] + epsilon, r_range_[1] - epsilon, 300)
        xi_ = xi(M, a, r_)
        eta_ = eta(M, a, r_)

        x = alpha(theta_O, xi_)
        y = beta(a, theta_O, xi_, eta_)

        x_ = x[np.logical_not(np.isnan(y))]
        y_ = y[np.logical_not(np.isnan(y))]

        X = np.concatenate(( np.flip(x_), x_, np.array([x_[-1]]) ))
        Y = np.concatenate(( np.flip(y_), -y_, np.array([y_[-1]]) ))

        # output
        f = open(str(M) + "_" + str(a) + ".dat", "w")
        f.truncate()
        f.write("alpha beta \n")
        for i in range(0, len(X)):
            f.write(str(X[i]))
            f.write(" ")
            f.write(str(Y[i]))
            f.write("\n")

        # plot
        plt.gca().set_aspect('equal')

        plt.plot(X, Y)

    plt.show()


# polar case
def Polar():
    
    metrics = [
        [ 1.0, 0.1 ],
        [ 1.0, 0.2 ],
        [ 1.0, 0.3 ],
        [ 1.0, 0.4 ],
        [ 1.0, 0.5 ],
        [ 1.0, 0.6 ],
        [ 1.0, 0.7 ],
        [ 1.0, 0.8 ],
        [ 1.0, 0.9 ],
        [ 1.0, 1.0 ]
    ]    

    for j in range(0, len(metrics)):
        M = metrics[j][0]
        a = metrics[j][1]

        print(polar_radius(M, a))



# plots as function of a and theta_O
def a_theta_O_plots():
     
    M = 1
    a = np.linspace(0, 1-epsilon, 11)
    theta_O = np.linspace(0, np.pi/2, 300)

    disp_fig = plt.figure(1)
    
    plt.xlabel("$\\theta_O$")
    plt.ylabel("$D$")
   
    delta_alpha_fig = plt.figure(2)
    
    plt.xlabel("$\\theta_O$")
    plt.ylabel("$\\Delta\\alpha$")   

    colors = [ cm.jet(x) for x in np.linspace(0, 1, len(a)) ]
        
    for i, a_ in enumerate(a):
        
        disp_f = open("shadow_offset_" + str(i) + ".dat", "w")
        disp_f.truncate()
        disp_f.write("theta D\n")
        
        delta_alpha_f = open("delta_alpha_" + str(i) + ".dat", "w")
        delta_alpha_f.truncate()
        delta_alpha_f.write("theta delta_alpha\n")
        
        delta_beta_f = open("delta_beta_" + str(i) + ".dat", "w")
        delta_beta_f.truncate()
        delta_beta_f.write("theta delta_beta\n")

        D_ = []
        delta_alpha_ = []
        delta_beta_ = []   

        for theta_O_ in theta_O:
            D_.append(D(M, a_, theta_O_))
            delta_alpha_.append(delta_alpha(M, a_, theta_O_))
            delta_beta_.append(delta_beta(M, a_, theta_O_))
        D_ = np.nan_to_num(np.array(D_), nan=0.0)
        delta_alpha_ = np.nan_to_num(np.array(delta_alpha_), nan=polar_radius(M, a_))
        delta_alpha_[delta_alpha_ > 1e10] = polar_radius(M, a_)
        delta_beta_ = np.nan_to_num(np.array(delta_beta_), nan=2*polar_radius(M, a_))

        for j, theta_O_ in enumerate(theta_O):
            disp_f.write(str(theta_O_) + " " + str(D_[j]) + "\n")
            delta_alpha_f.write(str(theta_O_) + " " + str(delta_alpha_[j]) + "\n")
            delta_beta_f.write(str(theta_O_) + " " + str(delta_beta_[j]) + "\n")
        disp_f.close()
        delta_alpha_f.close()
        delta_beta_f.close()

        plt.figure(1)
        plt.plot(theta_O, D_, color=colors[i])
        plt.figure(2)
        plt.plot(theta_O, delta_alpha_, color=colors[i])       
        plt.figure(3)
        plt.plot(theta_O, delta_beta_, color=colors[i])

    plt.show()




# D and delta_alpha heatmap as a function of a and theta_O
def a_theta_O_heatmaps():
    N = 100
    i, j = np.meshgrid(np.arange(0, N), np.arange(0, N))
    
    M = 1
    a = np.linspace(0, 1-epsilon, N)
    theta_O = np.linspace(0, np.pi/2, N)

    a_grid, theta_O_grid = np.meshgrid(a, theta_O)

    disp_f = open("shadow_offset_hm.dat", "w")
    disp_f.truncate()
    disp_f.write("a theta D\n")
        
    delta_alpha_f = open("shadow_delta_alpha_hm.dat", "w")
    delta_alpha_f.truncate()
    delta_alpha_f.write("a theta delta_alpha\n")
    
    delta_beta_f = open("shadow_delta_beta_hm.dat", "w")
    delta_beta_f.truncate()
    delta_beta_f.write("a theta delta_beta\n")

    disp_fig = plt.figure(1)    
    plt.xlabel("$a$")
    plt.ylabel("$\\theta_O$")
   
    delta_alpha_fig = plt.figure(2)  
    plt.xlabel("$a$")
    plt.ylabel("$\\theta_O$")    

    delta_beta_fig = plt.figure(3)
    plt.xlabel("$a$")
    plt.ylabel("$\\theta_O$")

    D_ = np.empty_like(theta_O_grid)
    delta_alpha_ = np.empty_like(theta_O_grid)
    delta_beta_ = np.empty_like(theta_O_grid)
   
    for (i_, j_) in zip(i.flatten(), j.flatten()):
        D_[i_,j_] = D(M, a_grid[i_,j_], theta_O_grid[i_,j_])
        if np.isnan(D_[i_,j_]):
            D_[i_,j_] = 0
        
        delta_alpha_[i_,j_] = delta_alpha(M, a_grid[i_,j_], theta_O_grid[i_,j_])
        if np.isnan(delta_alpha_[i_,j_]) or delta_alpha_[i_,j_] > 1e10:
            delta_alpha_[i_,j_] = polar_radius(M, a_grid[i_,j_])

        delta_beta_[i_,j_] = delta_beta(M, a_grid[i_,j_], theta_O_grid[i_,j_])
        if np.isnan(delta_beta_[i_,j_]) or delta_beta_[i_,j_] > 1e10:
            delta_beta_[i_,j_] = polar_radius(M, a_grid[i_,j_])

        disp_f.write(str(a_grid[i_,j_]) + " " + str(theta_O_grid[i_,j_]) + " " + str(D_[i_,j_]) + "\n")
        delta_alpha_f.write(str(a_grid[i_,j_]) + " " + str(theta_O_grid[i_,j_]) + " " + str(delta_alpha_[i_,j_]) + "\n")
        delta_beta_f.write(str(a_grid[i_,j_]) + " " + str(theta_O_grid[i_,j_]) + " " + str(delta_beta_[i_,j_]) + "\n")

    plt.figure(1)
    c1 = plt.contourf(a_grid, theta_O_grid, D_, vmin=0, vmax=2.5, levels=10)
    plt.colorbar(c1)

    plt.figure(2)
    c2 = plt.contourf(a, theta_O, delta_alpha_, levels=10)
    plt.colorbar(c2)

    plt.figure(3)
    c3 = plt.contourf(a_grid, theta_O_grid, delta_beta_, levels=10)
    plt.colorbar(c3)

    plt.show()



# contours for circle fit approach
def circle_fit_contour():
    N = 100
    i, j = np.meshgrid(np.arange(0, N), np.arange(0, N))
    
    M = 1
    a = np.linspace(0, 1-epsilon, N)
    theta_O = np.linspace(0, np.pi/2, N)

    a_grid, theta_O_grid = np.meshgrid(a, theta_O)

    R_S_f = open("R_S_hm.dat", "w")
    R_S_f.truncate()
    R_S_f.write("a theta R_S\n")
    
    delta_S_f = open("delta_S_hm.dat", "w")
    delta_S_f.truncate()
    delta_S_f.write("a theta delta_S\n")

    R_S_fig = plt.figure(1)    
    plt.xlabel("$a$")
    plt.ylabel("$\\theta_O$")
   
    delta_S_fig = plt.figure(2)  
    plt.xlabel("$a$")
    plt.ylabel("$\\theta_O$")    

    R_S_ = np.empty_like(theta_O_grid)
    delta_S_ = np.empty_like(theta_O_grid)
   
    for (i_, j_) in zip(i.flatten(), j.flatten()):
        R, delta = R_S_delta_S(M, a_grid[i_,j_], theta_O_grid[i_,j_])

        R_S_[i_,j_] = R
        delta_S_[i_,j_] = delta

        R_S_f.write(str(a_grid[i_,j_]) + " " + str(theta_O_grid[i_,j_]) + " " + str(R_S_[i_,j_]) + "\n")
        delta_S_f.write(str(a_grid[i_,j_]) + " " + str(theta_O_grid[i_,j_]) + " " + str(delta_S_[i_,j_]) + "\n")
 
    plt.figure(1)
    c1 = plt.contourf(a_grid, theta_O_grid, R_S_, levels=10)
    plt.colorbar(c1)

    plt.figure(2)
    c1 = plt.contourf(a_grid, theta_O_grid, delta_S_, levels=10)
    plt.colorbar(c1)
    
    plt.show()

# contours for curvature radii
def curvature_radii_contour():
    N = 200
    i, j = np.meshgrid(np.arange(0, N), np.arange(0, N))
    
    M = 1
    a = np.linspace(0, 1-epsilon, N)
    theta_O = np.linspace(0, np.pi/2, N)

    a_grid, theta_O_grid = np.meshgrid(a, theta_O)

    R_CL_f = open("R_CL_hm.dat", "w")
    R_CL_f.truncate()
    R_CL_f.write("a theta R_CL\n")
 
    R_CR_f = open("R_CR_hm.dat", "w")
    R_CR_f.truncate()
    R_CR_f.write("a theta R_CR\n")

    R_CT_f = open("R_CT_hm.dat", "w")
    R_CT_f.truncate()
    R_CT_f.write("a theta R_CT\n") 

    R_CL_fig = plt.figure(1)    
    plt.xlabel("$a$")
    plt.ylabel("$\\theta_O$")
   
    R_CR_fig = plt.figure(1)    
    plt.xlabel("$a$")
    plt.ylabel("$\\theta_O$")
    
    R_CT_fig = plt.figure(2)    
    plt.xlabel("$a$")
    plt.ylabel("$\\theta_O$")
    
    R_CL_ = np.empty_like(theta_O_grid)
    R_CR_ = np.empty_like(theta_O_grid)
    R_CT_ = np.empty_like(theta_O_grid)
   
    for (i_, j_) in zip(i.flatten(), j.flatten()):
        R_CL, R_CR, R_CT = R_C_LRT(M, a_grid[i_,j_], theta_O_grid[i_,j_])

        R_CL_[i_,j_] = R_CL
        R_CR_[i_,j_] = R_CR
        R_CT_[i_,j_] = R_CT

        R_CL_f.write(str(a_grid[i_,j_]) + " " + str(theta_O_grid[i_,j_]) + " " + str(R_CL_[i_,j_]) + "\n")
        R_CR_f.write(str(a_grid[i_,j_]) + " " + str(theta_O_grid[i_,j_]) + " " + str(R_CR_[i_,j_]) + "\n")
        R_CT_f.write(str(a_grid[i_,j_]) + " " + str(theta_O_grid[i_,j_]) + " " + str(R_CT_[i_,j_]) + "\n")

    R_CL_[R_CL_ > 6] = 6
    plt.figure(1)
    c1 = plt.contourf(a_grid, theta_O_grid, R_CL_, levels=10) 
    plt.colorbar(c1)

    plt.figure(2)
    c1 = plt.contourf(a_grid, theta_O_grid, R_CR_, levels=10)
    plt.colorbar(c1)

    plt.figure(3)
    c1 = plt.contourf(a_grid, theta_O_grid, R_CT_, levels=10)
    plt.colorbar(c1)
    
    plt.show()



if __name__ == "__main__":
    #Shadows()
    #Polar()
    #a_theta_O_plots()
    #a_theta_O_heatmaps()
    #circle_fit_contour()
    curvature_radii_contour()
